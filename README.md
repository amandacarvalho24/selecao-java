<!DOCTYPE html>

<h1>Documentação Projeto Histórico de Preços de Combustíveis</h1>

<h3>Operações com Usuário</h3>
 
 <p><strong>GET</strong></p>
 
 <p>Lista todos os usuários</p>
 <p><i>/users</i></p>
 <p>Busca os usuários por ID</p>
 <p><i>/users/{id}</i></p>
 <p>Busca os usuários pelo nome de usuário</p>
 <p><i>/users/{username}</i></p>
 <p>Busca os usuários pelo primeiro nome</p>
 <p><i>/users/{firstname}</i></p>
 <p>Busca os usuários pelo sobrenome</p>
 <p><i>/users/{lastname}</i></p>
 <p>Busca os usuários pelo e-mail</p>
 <p><i>/users/{email}</i></p> 

<p><strong>POST</strong></p>
 
 <p>Salva um novo usuário</p>
 <p><i>/users/save</i></p>
 <p>É obrigatório informar:
  - Nome de usuário
  - Primeiro nome
  - E-mail</p>
   <p>Propriedades do Usuário: </p>
   
    properties:
      id:
        type: "integer"
      username:
        type: "string"
        example: "mariasilva01"
      firstName:
        type: "string"
        example: "Maria"
      lastName:
        type: "string"
        example: "Silva"
      email:
        type: "string"
        example: "mariasilva@email.com"
    xml:
      name: "User"
 
 <p><strong>PUT</strong></p>
 
 <p>Atualiza um usuário existente por meio do id</p>
 <p><i>/users/update/{id}</i></p> 
  
 <p><strong>DELETE</strong></p>
 
 <p>Exclui um usuário existente por meio do id</p>
 <p><i>/users/delete/{id}</i></p>
  
 <h3>Operações com Combustível</h3>
 
 <p><strong>POST</strong></p>
 <p>Salva uma nova entrada da coleta de preços de combustíveis</p>
 <p><i>/fuel/saveFuel</i></p>
  <p>É obrigatório informar todas as informações listadas nas propriedades abaixo:</p>
  <p>Propriedades da entrada de coleta de preços: </p>
      
      id:
        type: "integer"
      
      region:
        type: "string"
        example: "NE"
      
      state:
        type: "string"
        example: "Paraíba"
      
      town:
        type: "string"
        example: "João Pessoa"
      
      resale:
        type: "string"
        example: "Posto de Seu Biu"
      
      plant:
        type: "integer"
        example: "1754"
      
      product:
        type: "string"
        example: "Álcool"
      
      dateCollect:
        type: "string"
        example: "01/01/2020"
      
      purchasePrice:
        type: "integer"
        example: "4,50"
      
      salePrice:
        type: "integer"
        example: "5,15"
      
      mesUnit:
        type: "string"
        example: "R$ / litro"
      
      bussBanner:
        description: "Especificar a bandeira do posto"
        type: "string"
        example: "Petrobrás"  
    
    xml:
      name: "Fuel"
 
 <p>Salva um arquivo csv</p>
 <p><i>/fuel/fueldb</i></p>

<p><strong>GET</strong></p>

<p>Lista todas as entradas</p>
<p><i>/fuel/</i></p>
<p>Busca a entrada por meio do ID</p>
<p><i>/fuel/{id}</i></p>
<p>Entrega valor médio do preço de venda por cidade</p>
<p><i>/fuel/venda/{town}</i></p>
<p>Entrega valor médio do preço de compra por cidade</p>
<p><i>/fuel/compra/{town}</i></p>
<p>Entrega valor médio do preço de venda por bandeira</p>
<p><i>/fuel/venda/{bussBanner}</i></p>
<p>Entrega valor médio do preço de compra por bandeira</p>
<p><i>/fuel/compra/{bussBanner}</i></p> 
<p>Traz todos os dados por região</p>
<p><i>/fuel/regions/{region}</i></p> 
<p>Traz os dados agrupados por cidade</p>
<p><i>/fuel/towns</i></p> 
<p>Traz os dados agrupados por revenda</p>
<p><i>/fuel/resales</i></p> 
<p>Traz os dados agrupados por data de coleta dos preços</p>
<p><i>/fuel/dateCollect</i></p> 
 
<p><strong>PUT</strong></p>

<p>Atualiza uma entrada já existente por meio do ID</p>
<p><i>/fuel/update/{id}</i></p>
 
<p><strong>DELETE</strong></p>

<p>Apaga uma entrada por meio do ID</p>
<p><i>/fuel/delete/{id}</i></p>
 
  
